from django.contrib import admin
from sample.models import Samp_data, Experiment, Expe_type, Species, Expe_method, Sample_mixe, Sample_compo, Sample_strain

admin.site.register(Experiment)
admin.site.register(Sample_compo)
admin.site.register(Sample_strain)
admin.site.register(Sample_mixe)
admin.site.register(Expe_type)
admin.site.register(Expe_method)
admin.site.register(Species)
admin.site.register(Samp_data)
