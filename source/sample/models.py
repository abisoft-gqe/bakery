from django.db import models
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.db.models.deletion import CASCADE

from data.models import Data
from compo_process.models import Person,Component


class Expe_type(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField(max_length=500)
    def __unicode__(self):
        return self.name
    

class Expe_method(models.Model):
    name = models.CharField(max_length=50, unique=True)
    type = models.ForeignKey(Expe_type, on_delete=CASCADE)
    description = models.TextField(max_length=500)
    def __unicode__(self):
        return "{0} with type {1}".format(self.name,self.type)
    

class Species(models.Model):
    name = models.CharField(max_length=50, unique=True)
    def __unicode__(self):
        return self.name


class Experiment(models.Model):
    name = models.CharField(max_length=50, unique=True)
    type = models.ForeignKey(Expe_type, on_delete=CASCADE)
    method = models.ForeignKey(Expe_method, on_delete=CASCADE)
    experimenter = models.ForeignKey(Person, on_delete=CASCADE)
    time_start = models.DateField(blank=True, default=timezone.make_aware(timezone.datetime.min, timezone.get_default_timezone()))
    time_tot = models.FloatField(blank=True)
    quantity_used = models.FloatField(blank=True)
    def __unicode__(self):
        return "{0} with type {1} started at {2}".format(self.name,self.type,self.time_start)
    def clean(self):
        if self.quantity_used < 0:
            raise ValidationError('erreur : quantite negative')
    class Meta:
        ordering = ['time_start']


class Sample(models.Model):
    name = models.CharField(max_length=50, unique=True)
    type = models.CharField(max_length=50, blank=True)
    ori_component = models.ForeignKey(Component, on_delete=CASCADE)
    quantity_init = models.FloatField(blank=True)
    date_creation = models.DateField(blank=True, default=timezone.make_aware(timezone.datetime.min, timezone.get_default_timezone()))
    #ori_experiment ne represente pas forcement experience origine
    ori_experiment = models.ManyToManyField(Experiment)
    def __unicode__(self):
        #preciser nom et origine (component ou expermient
        return self.name
    def clean(self):
        #verification de la validite de la quantite
        if self.quantity_init < 0:
            raise ValidationError('erreur : quantite negative')
    class Meta:
        ordering = ['date_creation']


class Samp_data(models.Model):
    sample = models.ForeignKey(Sample, on_delete=CASCADE)
    data = models.ForeignKey(Data, on_delete=CASCADE)
    experiment = models.ForeignKey(Experiment, on_delete=CASCADE)


class Sample_compo(Sample):
    component = models.ForeignKey('compo_process.component', on_delete=CASCADE)

#definir table Medium pour Sample_strain et Sample_mixe
class Sample_strain(Sample):
    strain = models.ForeignKey(Species, on_delete=CASCADE)
    medium = models.CharField(max_length=50, unique=True)


class Sample_mixe(Sample):
    medium = models.CharField(max_length=50, unique=True)





