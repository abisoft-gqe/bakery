'''
Created on 3 juil. 2015

@author: jocelyn
'''


from django.db import models



class TypeManager(models.Manager):
    def create_Type(self,name,description):
        Type = self.create(name,description)
        return Type


class MethodManager(models.Manager):
    def create_Method(self,name,new_type,description):
        from sample.models import Type
        Type = Type.objects.get(name=new_type)
        Method = self.create(name,Type,description)
        return Method

class SpeciesManager(models.Manager):
    def create_Species(self,name):
        Species = self.create(name)
        return Species








