'''
Created on 2 juil. 2015

@author: jocelyn
'''

from django.db import models


class Compo_typeManager(models.Manager):
    def create_compo_type(self,compo_type,unite):
        Compo_type=self.create(compo_type, unite)
        return Compo_type

class ComponentManager(models.Manager):
    def create_component(self,name,new_compo_type,quantity_init):
        from compo_process.models import Compo_type
        compo_type = Compo_type.objects.get(compo_type=new_compo_type)
        component = self.create(name, compo_type, quantity_init)
        return component

class Method_processManager(models.Manager):
    def create_Method_process(self,name,process_type,description_method):
        Method_process = self.create(name, process_type, description_method)
        return Method_process

class ProcessManager(models.Manager):
    def create_Process(self,name,new_type,method,time_start,time_tot):
        from compo_process.models import Type_process,Method_process
        type_process = Type_process.objects.get(name=new_type)
        method = Method_process.objects.get(name=method)
        Process = self.create(name, type_process, method, time_start, time_tot)
        return Process

class Compo_processManager(models.Manager):
    def create_Compo_process(self,new_father,new_son,new_process,quantity_use):
        from compo_process.models import Component
        from compo_process.models import Process
        compo_father = Component.objects.get(name=new_father)
        compo_son = Component.objects.get(name=new_son)
        process = Process.objects.get(name=new_process)
        Compo_process = self.create(compo_father, compo_son, process, quantity_use)
        return Compo_process

class Ext_factorManager(models.Manager):
    def create_Ext_factor(self,name,new_component,quantity_loss,reason,date):
        from compo_process.models import Component
        component = Component.objects.get(name=new_component)
        Ext_factor = self.create(name, component, quantity_loss, reason, date)
        return Ext_factor




