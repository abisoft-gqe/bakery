from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError

from data.models import Data
from django.db.models.deletion import CASCADE


class Compo_type(models.Model):
    UNIT = (
            ('g','gramme'),
            ('L','litre'),
            ('c','comptage'),
    )
    compo_type = models.CharField(max_length=50, unique=True)
    unite = models.CharField(max_length=1,choices=UNIT)
    def __unicode__(self):
        return self.compo_type
    class Meta:
        ordering = ['compo_type']

class Component(models.Model):
    name = models.CharField(max_length=50, unique=True)
    compo_type = models.ForeignKey(Compo_type, on_delete=CASCADE)
    quantity_init = models.FloatField(blank=True)
    def __unicode__(self):
        return self.name
    def clean(self):
        #verification de la validite de la quantite renseignee
        if self.quantity_init < 0:
            raise ValidationError('erreur : quantite negative')
    class Meta:
        ordering = ['compo_type']


class Type_process(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description_type = models.TextField(max_length=1000, default='none')
    def __unicode__(self):
        return self.name
    class Meta:
        ordering = ['name']


class Person(models.Model):
    name=models.CharField(max_length=50, unique=True)
    def __unicode__(self):
        return self.name


class Process(models.Model):
    person = models.ManyToManyField(Person)
    type_process = models.ForeignKey(Type_process, on_delete=CASCADE)
    time_start = models.DateField(blank=True, default=timezone.make_aware(timezone.datetime.min, timezone.get_default_timezone()))
    time_tot = models.IntegerField(blank=True)
    def __unicode__(self):
        return "{0} process started at {1}".format(self.type_process,self.time_start)
    class Meta:
        ordering = ['time_start']
    def clean(self):
        if not self.type_process == self.method_process.process_type:
            raise ValidationError('erreur : method et type non compatibles')


class Compo_process(models.Model):
    #relier les data aux component ou aux process plutot qu aux compo_process ?
    compo_father = models.ForeignKey(Component, related_name='father_component', on_delete=CASCADE)
    compo_son = models.ForeignKey(Component, related_name='son_component', on_delete=CASCADE)
    process = models.ForeignKey(Process, on_delete=CASCADE)
    quantity_use = models.FloatField(blank=True)
    data = models.ManyToManyField(Data)
    class Meta:
        unique_together = ("compo_father","compo_son", "process")
        ordering = ['process']
    def clean(self):
        if self.quantity_use < 0:
            raise ValidationError('erreur : quantite negative')


class Process_bool(models.Model):
    type_process = models.ForeignKey(Type_process, on_delete=CASCADE)
    father_type = models.ForeignKey(Compo_type, related_name='father_component', on_delete=CASCADE)
    son_type = models.ForeignKey(Compo_type, related_name='son_component', on_delete=CASCADE)
    optionnal_compo = models.BooleanField(default=False)
    multiple = models.BooleanField(default=False)
    class Meta:
        unique_together = ("father_type","son_type","type_process")
        ordering = ['type_process']
    def __unicode__(self):
        return "{0} : {1} --> {2}".format(self.type_process.name,self.father_type.compo_type,self.son_type.compo_type)

class Ext_factor(models.Model):
    name = models.CharField(max_length=50, unique=True)
    component = models.ForeignKey(Component, on_delete=CASCADE)
    quantity_loss = models.FloatField()
    reason = models.CharField(max_length=200, blank=True)
    date = models.DateField(blank=True, default=timezone.make_aware(timezone.datetime.min, timezone.get_default_timezone()))
    def __unicode__(self):
        return self.name
    def clean(self):
        #verification de la validite de la quantite perdue
        if self.quantity_loss < 0:
            raise ValidationError('erreur : quantite negative')
    class Meta:
        ordering = ['component']


#class Link_shine(models.Model):
#    seed_lot_bakery = models.ForeignKey(Component)


