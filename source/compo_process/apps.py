from __future__ import unicode_literals

from django.apps import AppConfig


class Compo_ProcessConfig(AppConfig):
    name = 'compo_process'
