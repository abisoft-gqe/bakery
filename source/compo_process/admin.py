from django.contrib import admin
from compo_process.models import Compo_process,Compo_type,Process,Component,Type_process,Process_bool,Ext_factor,Person

#class compoAdmin(admin.ModelAdmin):
#    raw_id_fields = ("compo_type",)
#    fields = ['compo_type','quantity_init']

admin.site.register(Component)
admin.site.register(Process)
admin.site.register(Compo_process)
admin.site.register(Compo_type)
admin.site.register(Type_process)
admin.site.register(Process_bool)
admin.site.register(Ext_factor)
admin.site.register(Person)


