'''
Created on 1 juil. 2015

@author: jocelyn
'''

from django.db import models


class VariableManager(models.Manager):
    def create_variable(self,type_var,description):
        variable = self.create(type_var, description)
        return variable

class MethodManager(models.Manager):
    def create_method(self,name,method):
        method = self.create(name, method)
        return method

class DataManager(models.Manager):
    def create_data(self,name,new_var,new_sur,new_meth,value,date):
        from survey.models import Survey
        from models import Variable,Method
        variable = Variable.objects.get(type=new_var)
        survey = Survey.objects.get(name=new_sur)
        method = Method.objects.get(name=new_meth)
        data = self.create(name, variable, survey, method, value, date)
        return data

class ExpertiseManager(models.Manager):
    def create_expertise(self,name,new_ori,new_meth,data,date):
        from survey.models import Survey
        from models import Method
        ori_data = Survey.objects.get(name=new_ori)
        method = Method.objects.get(name=new_meth)
        expertise = self.create(name, ori_data, method, data, date)
        return expertise

class Samp_dataManager(models.Manager):
    def create_samp_data(self,new_sample,new_data,new_experiment):
        from sample.models import Sample,Experiment
        from data.models import Data
        sample = Sample.objects.get(name=new_sample)
        data = Data.objects.get(name=new_data)
        experiment = Experiment.objects.get(name=new_experiment)
        samp_data = self.create(sample, data, experiment)
        return samp_data

class Process_dataManager(models.Manager):
    def create_process_data(self,new_father_son,new_data):
        from data.models import Data
        from compo_process.models import Process
        father_son = Process.objects.get(name=new_father_son)
        data = Data.objects.get(name=new_data)
        process_data = self.create(father_son, data)
        return process_data
        







