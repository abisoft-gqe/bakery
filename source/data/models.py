from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.db.models.deletion import CASCADE, SET_NULL



class Variable(models.Model):
    type = models.CharField(max_length=50, unique=True)
    description = models.TextField(max_length=1000, blank=True)
    def __unicode__(self):
        return self.type
    class Meta:
        ordering = ['type']


class Method(models.Model):
    name = models.CharField(max_length=50, unique=True)
    unit = models.CharField(max_length=10)
    method = models.TextField(max_length=1000,blank=True)
    def __unicode__(self):
        return self.name
    class Meta:
        ordering = ['name']


class Data(models.Model):
    variable = models.ForeignKey(Variable, on_delete=CASCADE)
    survey = models.ForeignKey('survey.Survey', blank=True, null=True, on_delete=SET_NULL)
    method = models.ForeignKey(Method, null=True, blank=True, on_delete=SET_NULL)
    value = models.CharField(max_length=50)
    date = models.DateTimeField(blank=True, default=timezone.make_aware(timezone.datetime.min, timezone.get_default_timezone()))
    def __unicode__(self):
        return "{0} collect at {1}".format(self.variable,self.date)
    class Meta:
        ordering = ['date']
    

class Expertise(models.Model):
    name = models.CharField(max_length=50, unique=True)
    ori_data = models.ForeignKey(Data, on_delete=CASCADE)
    method = models.ForeignKey(Method, on_delete=CASCADE)
    new_data=models.CharField(max_length=100)
    date = models.DateTimeField(blank=True, default=timezone.make_aware(timezone.datetime.max, timezone.get_default_timezone()))
    def __unicode__(self):
        return "{0} comoing from {1}".format(self.name,self.ori_data)
    def clean(self):
        #verification de la valisite de la date de la donnee expertisee
        if self.date < self.ori_data.date:
            raise ValidationError('Erreur : la date de l''information expertisee est plus ancienne que l''information d''origine')
    class Meta:
        ordering = ['date']






