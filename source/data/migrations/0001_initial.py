# Generated by Django 2.0 on 2019-02-25 14:26

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(max_length=50)),
                ('date', models.DateTimeField(blank=True, default=datetime.datetime(1, 1, 1, 0, 0, tzinfo=utc))),
            ],
            options={
                'ordering': ['date'],
            },
        ),
        migrations.CreateModel(
            name='Expertise',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
                ('new_data', models.CharField(max_length=100)),
                ('date', models.DateTimeField(blank=True, default=datetime.datetime(9999, 12, 31, 23, 59, 59, 999999, tzinfo=utc))),
            ],
            options={
                'ordering': ['date'],
            },
        ),
        migrations.CreateModel(
            name='Method',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
                ('unit', models.CharField(max_length=10)),
                ('method', models.TextField(blank=True, max_length=1000)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Variable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(max_length=50, unique=True)),
                ('description', models.TextField(blank=True, max_length=1000)),
            ],
            options={
                'ordering': ['type'],
            },
        ),
        migrations.AddField(
            model_name='expertise',
            name='method',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='data.Method'),
        ),
        migrations.AddField(
            model_name='expertise',
            name='ori_data',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='data.Data'),
        ),
        migrations.AddField(
            model_name='data',
            name='method',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='data.Method'),
        ),
    ]
