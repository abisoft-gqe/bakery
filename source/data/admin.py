from django.contrib import admin
from data.models import Variable, Data, Method, Expertise


admin.site.register(Variable)
admin.site.register(Data)
admin.site.register(Method)
admin.site.register(Expertise)

