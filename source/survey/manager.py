'''
Created on 3 juil. 2015

@author: jocelyn
'''
from django.db import models


class TypeManager(models.Manager):
    def create_type(self,type_survey):
        type_sur=self.create(type_survey)
        return type_sur

class PlanManager(models.Manager):
    def create_plan(self,name,plan,Type):
        from survey.models import Plan
        type_survey = Plan.objects.get(type=Type)
        plan=self.create(name,plan,type_survey)
        return plan

class SurveyManager(models.Manager):
    def create_survey(self,name,plan,date):
        from survey.models import Plan
        plan_survey = Plan.objects.get(plan=plan)
        survey=self.create(name, plan_survey, date)
        return survey
