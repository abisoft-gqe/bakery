from django.contrib import admin
from survey.models import Type,Plan,Survey,Person_survey

#class compoAdmin(admin.ModelAdmin):
#    raw_id_fields = ("compo_type",)
#    fields = ['compo_type','quantity_init']

admin.site.register(Type)
admin.site.register(Plan)
admin.site.register(Survey)
admin.site.register(Person_survey)