from django.db import models
from django.utils import timezone
from django.db.models.deletion import CASCADE

from data.models import Variable
from compo_process.models import Component,Person



class Type(models.Model):
    type = models.CharField(max_length=50, unique=True)
    def __unicode__(self):
        return self.type
    class Meta:
        ordering = ['type']


class Plan(models.Model):
    name = models.CharField(max_length=50, unique=True)
    plan = models.TextField(max_length=1000)
    type = models.ForeignKey(Type, on_delete=CASCADE)
    variable = models.ManyToManyField(Variable)
    def __unicode__(self):
        return "{0} with type {1}".format(self.name,self.type)
    class Meta:
        unique_together = ("plan","type")
        order_with_respect_to = 'type'


class Survey(models.Model):
    name = models.CharField(max_length=50, unique=True)
    plan = models.ForeignKey(Plan, on_delete=CASCADE)
    date = models.DateField(blank=True, default=timezone.make_aware(timezone.datetime.min, timezone.get_default_timezone()))
    component = models.ManyToManyField(Component)
    def __unicode__(self):
        return "{0} with type {1}".format(self.name,self.type)
    class Meta:
        ordering = ['date']


class Person_survey(models.Model):
    ROLE_SURVEY = (
        ('C', 'customer'),
        ('B', 'baker'),
    )
    survey = models.ForeignKey(Survey, on_delete=CASCADE)
    person = models.ForeignKey(Person, on_delete=CASCADE)
    role = models.CharField(max_length=1,choices=ROLE_SURVEY)
    def __unicode__(self):
        return "actor of survey {0}".format(self.survey)


