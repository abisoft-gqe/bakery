**************************************
Autodocumentation du projet ANR BAKERY
**************************************

csv_read.py
===========

Ce script contient les fonctions utilisees par le script read_process.py pour lire un fichier .csv lui-meme issu du script create process.py.

.. autofunction:: csv_read.create_component

.. autofunction:: csv_read.create_compo_process

.. autofunction:: csv_read.create_process

.. autofunction:: csv_read.create_data

.. autofunction:: csv_read.create_variable

.. autofunction:: csv_read.find_component

.. autofunction:: csv_read.find_compo_opt

.. autofunction:: csv_read.find_type_process

.. autofunction:: csv_read.read_process_file

.. autofunction:: csv_read.read_process_file

.. autofunction:: csv_read.split_values

.. autofunction:: csv_read.verify_nb_compo_opt

.. autofunction:: csv_read.verify_quantities

csv_create.py
=============

Ce script contient les fonctions utilisees par le script create_process.py pour creer un fichier .csv permettant de renseigner un nouveau process dans la base.

.. autofunction:: csv_create.define_variable

.. autofunction:: csv_create.search_compo_of_process

.. autofunction:: csv_create.show_type_process

.. autofunction:: csv_create.show_type_variable

.. autofunction:: csv_create.verification_compo

.. autofunction:: csv_create.verification_variable

.. autofunction:: csv_create.write_process_file
