import sys
from optparse import OptionParser
from csv_create import search_compo_of_process, write_process_file, show_type_process, show_type_variable,verification_compo,define_variable

#attention, a partir de la vesrion 1.7, il faut passer de optparse en argpase

parser = OptionParser()

parser.add_option("-f", "--file",
                   action="store", type="string", dest="filename", default="test.csv",
                   help="Indiquer le nom du fichier que vous voulez creer")

parser.add_option("-p", "--process",
                   action="store", type="string", dest="type_process",
                   help="Indiquer le nom du type de transformation que vous voulez enregistrer")

parser.add_option("-t", "--list_type",
                  action="callback", callback=show_type_process,
                  help="Montre la liste des types de transformations definis dans la base")

parser.add_option("-v", "--list_variable",
                  action="callback", callback=show_type_variable,
                  help="Montre la liste des variables definies dans la base")

(options, args) = parser.parse_args()

# priorite automatique a -h et a -l

if options.filename and not options.type_process:
    print 'erreur : type de process non renseigne'
    enter_process=raw_input("Nom du type de processus que vous voulez renseigner :\n")
else:
    enter_process=options.type_process
#######################################################################

#recherche des types de component lies au type de process
father,son,compo_opt=search_compo_of_process(enter_process)
while not father or not son:
    print'Type de process non existant'
    enter_process=raw_input("Nom du type de processus que vous voulez renseigner (n pour quitter) :\n")
    if enter_process=='n':
        sys.exit(0)
    father,son,compo_opt=search_compo_of_process(enter_process)

#ajout de component optionnels
compo_optionnel=list()
enter_compo = raw_input("Voulez-vous entrer un ingredient optionnel ? (o/n, tapez n pour arreter la saisie)\n")
while enter_compo == 'o':
    new_compo = raw_input("Nom du type d'ingredient (tapez n pour quitter) :\n")
    if new_compo == 'n':
        break
    else:
        valid_component = verification_compo(new_compo,compo_opt)
        if not valid_component:
            continue
        compo_optionnel.append(new_compo)

#entree au clavier des variables liees au process
#attetion : si on rentre une variable deja existance, on ne propose pas de creer de nouvelle variable et on garde donc la meme description de variable
variable=define_variable()

#creation du fichier
write_process_file(father,son,variable,compo_optionnel,options.filename)






