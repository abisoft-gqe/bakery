'''
Created on 24 juin 2015

@author: jocelyn
'''
import sys
from compo_process.models import Process_bool, Compo_type, Type_process
from data.models import Variable

def define_variable():
    """
    Demande a l'utilisateur de renseigner les variables qu'il veut definir dans un fichier, recherche si ces variables existes dans la base et, si non, propose de les creer 
    
    :return: variable: liste des variables demandees pas l'utilisateur
    :rtype: list [str]
    """
    variable=list()
    enter_variable = raw_input("Voulez-vous entrer une variable ? (o/n, tapez n pour arreter la saisie)\n")
    while enter_variable == 'o':
        new_variable = raw_input("Nom de la variable (tapez n pour quitter) :\n")
        if new_variable == 'n':
            break
        else:
            verification_variable(new_variable)
            variable.append(new_variable)
    return variable

def search_compo_of_process(type_process_recherche):
    """
    Recherche des types d'ingredients (objet compo_type) en foction du noms du type de process (note : cette fonction est utilisee dans create_process.py et read_process.py
    
    :param type_process_recherche: nom du type de process
    :type type_process_recherche: str
    :return: compo_father: liste des types d'ingredients peres pour ce type de process
    :rtype: list d'objet compo_type
    :return: compo_son: liste des types d'ingredients fils pour ce type de process
    :rtype: list d'objet compo_type
    :return: compo_opt: liste des types d'ingredients optionnels pour ce type de process
    :rtype: list d'objet compo_type
    """
    ids = Process_bool.objects.values_list('father_type',flat=True).filter(type_process__name=type_process_recherche,optionnal_compo=False)
    compo_father =Compo_type.objects.filter(pk__in=set(ids))
    ids = Process_bool.objects.values_list('father_type',flat=True).filter(type_process__name=type_process_recherche,optionnal_compo=True)
    compo_opt =Compo_type.objects.filter(pk__in=set(ids))
    ids = Process_bool.objects.values_list('son_type',flat=True).filter(type_process__name=type_process_recherche,optionnal_compo=False)
    compo_son = Compo_type.objects.filter(pk__in=set(ids))
    
    return compo_father,compo_son,compo_opt


def show_type_process(option, opt_str, value, parser):
    """
    Affichage des types de process disponibles dans la base (option du script create_process)
    """
    process_list=Type_process.objects.values_list('name',flat=True)
    for i in process_list:
        print i
    sys.exit(0)


def show_type_variable(option, opt_str, value, parser):
    """
    Affichage des types de variable disponibles dans la base (option du script create_process)
    """
    variable_list=Variable.objects.values_list('type',flat=True)
    for i in variable_list:
        print i
    sys.exit(0)


def verification_compo(new_compo,compo_opt):
    """
    Verification que l'ingredient optionnel fait bien parti des ingredients optionnels associes a ce process
    
    :param new_compo: nouvel ingredient entre par l'utilisateur
    :type new_compo: str
    :param compo_opt: liste des ingredients optionnels lies au process
    :type compo_opt: list [str]
    :return: booleen en fonction de si l'ingredient existe dans la base et de si il est bien associe a ce process
    :rtype: bool
    """
    try:
        compo = Compo_type.objects.get(compo_type=new_compo)
    except:
        print "Ce type d'ingredient n'existe pas"
        return False
        
    if compo not in compo_opt:
        print "erreur : cet ingredient ne fait pas parti des ingredients optionnels de ce process"
        return False
    return True


def verification_variable(new_var):
    """
    Verification que la variable existe dans la base, si non, proposition de la creer
    
    :param new_compo: nouvel ingredient entre par l'utilisateur
    :type new_compo: str
    :param compo_opt: liste des ingredients optionnels lies au process
    :type compo_opt: list [str]
    """
    var = Variable.objects.values_list('type',flat=True)
    if new_var not in var:
        choice = raw_input("Cette variable n'existe pas, voulez-vous la creer ? (o/n)")
        if choice == 'o':
            description = raw_input("Entrez une description de la variable :\n")
            v=Variable(type=new_var,description=description)
            v.save()
        else:
            print ("Variable non cree")
            sys.exit(0)


def write_process_file(father,son,variable,compo_optionnel,dest):
    """
    Ecriture du fichier csv
    
    :param father: types d'ingredients peres du process
    :type father: list objet Compo_type
    :param son: types d'ingredients fils du process
    :type son: list [str]
    :param compo_optionnel: types d'ingredients optionnels du process
    :type compo_optionnel: list [str]
    :param dest: Nom du fichier de destination
    :type dest: str
    """
    csvfile = open(dest,'w')
    for i in father:
        csvfile.write(i.compo_type + '\t' + 'quantite utilisee' + '\t')
    for i in compo_optionnel:
        csvfile.write(i + '\t' + 'quantitee utilisee' + '\t')
    for i in son:
        csvfile.write(i.compo_type + '\t' + 'quantitee produite' + '\t')
    csvfile.write('date de debut (JJ/MM/AAAA)'+ '\t' +'duree du process (min)'+ '\t' + 'acteur' + '\t')
    for i in variable:
        csvfile.write( i + '\t' + 'unite de : ' + i + '\t' + 'methode de mesure : ' + i + '\t' + 'description variable : ' + i + '\t' + 'date de mesure (AAAA-MM-DD HH:MM:SS) de : ' + i + '\t')

    csvfile.close()

