'''
Created on 24 juin 2015

@author: jocelyn
'''

import csv,sys,datetime
from compo_process.models import Component,Process,Type_process,Compo_process,Compo_type,Process_bool,Person
from data.models import Variable,Data,Method


def create_component(component,component_type_str,quantity):
    """
    Creation d'un ingredient
    
    :param component: le nom de l'ingredient
    :type component: str
    :param component_type_str: le type de l'ingredient
    :type component_type_str: str
    :param quantity: la quantite d'ingredient cree
    :type quantity: int
    """
    component_type_object=Compo_type.objects.get(compo_type=component_type_str)
    new_component=Component(name=component,compo_type=component_type_object,quantity_init=quantity)
    new_component.save()    


def create_compo_process(compo_son,compo_father,process,father_quantities,list_data):
    """
    Creation d'un objet compo_process correspondant a la relation entre un ingredient pere et un ingredient fils

    :param compo_son: l'ingredient fils
    :type compo_son: objet type Component
    :param compo_father: l'ingredient pere
    :type compo_father: objet type Component
    :param process: le process utilise lors de cette relation
    :type process: objet type Process
    :param father_quantities: la quantite d'ingredient pere utilise
    :type father_quantities: float
    :return: rien
    :rtype: aucun
    """
    for i in range(0,len(compo_son)):
        for n in range(0,len(compo_father)):
            compo_and_process=Compo_process(compo_father=compo_father[n],compo_son=compo_son[i],process=process,quantity_use=father_quantities[n])
            compo_and_process.save()
            for data in list_data:
                compo_and_process.data.add(data)
            


def create_process(type_of_process,t_start,time_total,actor):
    """
    Creation d'un objet process
    
    :param type_of_process: le type de process a creer
    :type type_of_process: objet Type_process
    :param method: la method du process
    :type method: objet Method_process
    :param t_start: la date de realisation du process
    :type t_start: objet datefield
    :param time_total: la duree de realisation du process
    :type time_total: int
    :return: process
    :rtype: objet process
    """
    exist_process = Process.objects.filter(type_process=type_of_process,time_start=t_start,time_tot=time_total)
    if not exist_process:
        person = Person.objects.get(name=actor)
        if not person:
            person=Person(name=actor)
            person.save()
        process=Process(type_process=type_of_process,time_start=t_start,time_tot=time_total)
        process.save()
        process.person.add(person)
        
    else:
        print "Ce processus existe deja"
        sys.exit(0)
    return process


def create_data(name_variable,variable):
    """
    Creation d'un objet data
    
    :param name_variable: nom de la variable associee a la donnee
    :type name_variable: str
    :param variable: la variable associee a cette donnee
    :type variable: objet Variable
    :return: new_data
    :rtype: objet Data
    """
    exist_variable = Variable.objects.filter(type=name_variable)
    exist_method = Method.objects.filter(name=variable[2])
    
    date = datetime.datetime.strptime(variable[4],"%d/%m/%Y %H:%M:%S")
    variable[4]=date
    
    if not exist_variable:
        print "erreur, la variable %s n'existe pas" % name_variable
        sys.exit(0)
    else:
        type_variable = Variable.objects.get(type=name_variable)
    
    if not exist_method:
        method_variable = Method.objects.create(name=variable[2],unit=variable[1],method=variable[3])
        method_variable.save()
    else:
        method_variable=Method.objects.get(name=variable[2])
    
    exist_data = Data.objects.filter(variable=type_variable,method=method_variable,value=variable[0],date=variable[4])
    if not exist_data:
        new_data = Data.objects.create(variable=type_variable,method=method_variable,value=variable[0],date=variable[4])
        new_data.save()
    else:
        print "erreur : donnee deja existante"
        sys.exit(0)
    return new_data


def create_variable(nb_compo_father,nb_compo_son,column_name,variable):
    """
    Creation des objets Variable et des objets Data associes
    
    :param nb_compo_father: nombre d'ingredient pere
    :type nb_compo_father: int
    :param nb_compo_son: nombre d'ingredient fils
    :type nb_compo_son: int
    :param column: numero de colonne du fichier
    :type column: int
    :param variable: la variable associee a cette donnee
    :type variable: objet Variable
    :return: new_data
    :rtype: objet Data
    """
    variable_begin=(nb_compo_father+nb_compo_son)*2+3
    list_data = list()
    for x in xrange(0,len(variable),5):
        sub_variable = variable[x:x+5]
        name_variable = column_name[variable_begin+x]
        new_data = create_data(name_variable,sub_variable)
        list_data.append(new_data)
    return list_data


def find_component(father_components,son_components):
    """
    Recherche des objets Component des ingredients peres et fils renseignes
    
    :param father_components: liste des noms des ingredients peres
    :type father_components: list['str']
    :param son_components: liste des noms des ingredients fils
    :type son_components: list['str']
    :return: compo_father
    :rtype: objet Component
    :return: compo_son
    :rtype: objet Component
    """
    compo_father=list()
    for name_compo in father_components:
        exist_compo = Component.objects.get(name=name_compo)
        if not exist_compo:
            print "Ingredient pere non trouve"
            sys.exit(0)
        else:
            compo_father.append(Component.objects.get(name=name_compo))

    compo_son=list()
    for name_compo in son_components:
        exist_compo = Component.objects.get(name=name_compo)
        if not exist_compo:
            print "Ingredient fils non trouve"
            sys.exit(0)
        else:
            compo_son.append(Component.objects.get(name=name_compo))
    return compo_father,compo_son


def find_compo_opt(name_compo_opt):
    """
    Verification de l'existance des ingredients optionnels peres renseignes
    
    :param name_compo_opt: liste des noms des ingredients peres
    :type name_compo_opt: list['str']
    :return: rien
    :rtype: aucun
    """
    ids = Component.objects.values_list('compo_type',flat=True).filter(name=name_compo_opt)
    compo_type = Compo_type.objects.filter(pk__in=set(ids))
    try:
        Process_bool.objects.get(father_type=compo_type, optionnal_compo=True)
    except:
        print "Erreur dans le fichier : l'ingredient optionnel %s n'est pas disponible pour ce process" %name_compo_opt


def find_type_process(name_type_process):
    """
    Recherche de l'objet Type_process renseigne
    
    :param name_type_process: type de process passe en parametre du script read_process
    :type name_type_process: 'str'
    :return: type_of_process
    :rtype: objet Type_process
    """
    type_of_process = Type_process.objects.get(name=name_type_process)
    if not type_of_process:
        print "Pas de type de process correspondant"
        sys.exit(0)
    return type_of_process


def read_process_file(name_file_process):
    """
    Lecture du fichier .csv renseignant des process, retourne les entetes de colonnes et les listes de valeurs (fonction a remplacer par un DictReader ?)
    
    :param name_file_process: nom du fichier a lire, renseigne en parametre de read_process
    :type name_file_process: 'str'
    :return: col
    :rtype: list ['str']
    :return: list_value_list
    :rtype: list [list ['str']]
    """
    try:
        file_process=open(name_file_process,"rb")
    except IOError:
        print "Erreur a l'ouverture du fichier"
        sys.exit(0)
    process=csv.reader(file_process,delimiter='\t')
    col=next(process)
    list_value_list=list()
    for row in process:
        list_value_list.append(row)
    
    file_process.close()
    return col,list_value_list


def split_values (value_list,nb_compo_father,nb_compo_son,nb_compo_opt,name_column):
    """
    Separation des donnees du fichier en differentes variables
    
    :param value_list: liste des donnes du fichier
    :type value_list: list ['str']
    :param nb_compo_father: nombre de component pere obligatoires pour le type process renseigne en parametre
    :type nb_compo_father: int
    :param nb_compo_son: nombre de component fils obligatoires pour le type process renseigne en parametre
    :type nb_compo_son: int
    :param nb_compo_opt: nombre de component optionnels renseigne en parametre
    :type nb_compo_opt: int
    :param name_column: liste des noms de colonnes du fichier
    :type name_column: list ['str']
    :return: father_components
    :rtype: list ['str']
    :return: son_components
    :rtype: list ['str']
    :return: father_quantities
    :rtype: list ['str']
    :return: son_quantities
    :rtype: list ['str']
    :return: process
    :rtype: list ['str']
    :return: variable
    :rtype: list ['str']
    """
    #attention : les ingredients peres doivent deja exister
    column=0
    father_components=list()
    son_components=list()
    father_quantities=list()
    son_quantities=list()
    for n in range(0,nb_compo_father):
        try:
            father_components.append(Component.objects.get(name=value_list[column]))
        except:
            print "Ingredient non renseigne"
            sys.exit(0)
        column+=1
        father_quantities.append(value_list[column])
        column+=1
    for n in range(0,nb_compo_opt):
        find_compo_opt(Component.objects.get(name=value_list[column]))
        try:
            father_components.append(Component.objects.get(name=value_list[column]))
        except:
            print "Ingredient non renseigne"
            sys.exit(0)
        column+=1
        father_quantities.append(value_list[column])
        column+=1
    for n in range(0,nb_compo_son):
        exist_compo_son = Component.objects.filter(name=value_list[column])
        if not exist_compo_son:
            create_component(value_list[column],name_column[column],value_list[column+1])
        son_components.append(Component.objects.get(name=value_list[column]))
        column+=1
        son_quantities.append(value_list[column])
        column+=1
    process=list()
    for n in range(column,column+3):
        process.append(value_list[n])
    column=column+3
    variable=list()
    for n in range(column,len(value_list)):
        variable.append(value_list[n])
    return father_components,son_components,father_quantities,son_quantities,process,variable

def verify_nb_compo_opt(name_column,nb_compo_father,nb_compo_son,nb_compo_opt):
    """
    Verification que le nombre d'ingredient renseigne dans le fichier correspond bien a la somme des ingredients obligatoires du process et des ingredient optionnels renseignes par l'utilisateur en parametre
    
    :param name_column: liste des noms de colonnes du fichier
    :type name_column: list ['str']
    :param nb_compo_father: nombre de component pere obligatoires pour le type process renseigne en parametre
    :type nb_compo_father: int
    :param nb_compo_son: nombre de component fils obligatoires pour le type process renseigne en parametre
    :type nb_compo_son: int
    :param nb_compo_opt: nombre de component optionnels renseigne en parametre
    :type nb_compo_opt: int
    """
    if "debut (JJ/MM/AAAA)" != name_column[(nb_compo_father+nb_compo_son+nb_compo_opt)*2]:
        print "Erreur : Le nombre d'ingredient dans le fichier ne correspond pas au nombre d'ingredients du type de process additionne du nombre d'ingredient optionnel renseigne. Verifiez si vous n'avez pas oublie de preciser le nombre d'ingredient optionnels."
        sys.exit(0)
    

def verify_quantities(compo_father,father_quantities):
    """
    Verification que la quantite preleve n'est pas superieur a la quantite initiale de l'ingredient
    
    :param compo_father: Ingredient pere a verifier
    :type compo_father: objet Component
    :param father_quantities: quantite d'ingredient pere preleve
    :type father_quantities: str
    """
    for n in range(0,len(compo_father)):
        if compo_father[n].quantity_init >= father_quantities[n]:
            print ("Attention : vous avez preleve une quantitee d'ingredient superieur a la quantite initiale")
            #verifier que la somme des prelevements n'est pas superieurs a la quantite initiale


