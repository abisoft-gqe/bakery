'''
Created on 15 juin 2015

@author: jocelyn
'''

import unittest

from create_process import requete_bool

class TestRequete_bool(unittest.TestCase):
    
    def test_requete_bool(self):
        TYPE_PROCESS='panification'
        father,son=requete_bool(TYPE_PROCESS)
        self.assertEqual(son, 'pain')
        



if __name__ == '__main__':
    unittest.main()