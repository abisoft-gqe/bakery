'''
Created on 25 juin 2015

@author: jocelyn
'''
import sys,datetime
from optparse import OptionParser
from csv_read import read_process_file,split_values,create_process,find_type_process,find_component,verify_quantities,create_variable,create_compo_process,verify_nb_compo_opt
from csv_create import search_compo_of_process


parser = OptionParser()

parser.add_option("-f", "--file",
                   action="store", type="string", dest="filename",
                   help="Indiquer le nom du fichier que vous voulez lire")

parser.add_option("-t", "--type",
                   action="store", type="string", dest="type_process",
                   help="Indiquer le nom du type de process que vous voulez enregistrer")

parser.add_option("-o",
                   type="int", dest="nb_compo_opt", default=0,
                   help="Indiquer le nombre d'ingredients optionnels")

(options, args) = parser.parse_args()

if options.filename and not options.type_process:
    print 'erreur : type de process non renseigne'
    sys.exit(0)

if options.type_process and not options.filename:
    print 'erreur : nom de fichier non renseigne'
    sys.exit(0)

#recherche des ingredients du type de process
compo_father,compo_son,compo_opt=search_compo_of_process(options.type_process)
if not compo_father or not compo_son:
    print'Type de process non existant'
    sys.exit(0)

#lecture du fichier csv
name_column,list_value_list = read_process_file(options.filename)

#comptage des component peres et fils
nb_compo_father = len(compo_father)
nb_compo_son = len(compo_son)

#necessite de verifier qu'il y a le bon nombre de compo_father (bon nombre de components optionnels renseignes
verify_nb_compo_opt(name_column,nb_compo_father,nb_compo_son,options.nb_compo_opt)

#recuperation des valeurs enregistrees dans le fichier
for value_list in list_value_list:
    father_components,son_components,father_quantities,son_quantities,process,variable=split_values(value_list,nb_compo_father,nb_compo_son,options.nb_compo_opt,name_column)
    date = datetime.datetime.strptime(process[0],"%d/%m/%Y").date()
    process[0]=date
    nb_compo_father = len(compo_father)+options.nb_compo_opt
    #trouver l'objet process type
    type_of_process = find_type_process(options.type_process)
    #creer process
    process = create_process(type_of_process,process[0],process[1],process[2])
    #retrouver component
    compo_father,compo_son = find_component(father_components,son_components)
    #verifier que les quantite utilisees ne sont pas sup aux quantite existantes
    verify_quantities(compo_father,father_quantities)
    #creation des variables (warning : naive date, sans doute car il faut aussi une heure)
    list_data = create_variable(nb_compo_father,nb_compo_son,name_column,variable)
    #creer compo_process
    create_compo_process(compo_son,compo_father,process,father_quantities,list_data)

    #attention : si l'utilisateur rentre plusieurs ingredient du meme type, verifier que multiple est a True dans Process_bool
    

        
    #le nom de l'acteur sera a enregistrer dans shinemas



