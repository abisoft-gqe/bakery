'''
Created on 25 juin 2015

@author: jocelyn
'''
from optparse import OptionParser
from csv_create import show_type_variable

from sample.models import Expe_type

parser = OptionParser()

parser.add_option("-f", "--file",
                   action="store", type="string", dest="filename", default="test.csv",
                   help="Indiquer le nom du fichier que vous voulez creer")

parser.add_option("-t", "--type",
                  action="store", type="string", dest="type_expe",
                  help="Indiquer le type d'experience")

parser.add_option("-l", "--list_variable",
                  action="callback", callback=show_type_variable,
                  help="Montre la liste des types de variables definis dans la base")

parser.add_option("-o", "--origine",
                  action="store", type="string", dest="origine", default="exist",
                  help="Indiquez 'new' si vous avez besoin de creer de nouveaux echantillons")

(options, args) = parser.parse_args()

fin_saisie_ech='n'
while options.origine =="new" or fin_saisie_ech=='o':
    new_ech=raw_input("Indiquez le nom du nouvel echantillon")
    #autre info necessaire pour crer un echantillon
    fin_saisie_ech=raw_input("Voulez vous saisir un autre nouvel echantillon ? (o/n)")

#verification des types d'experience
if options.filename and not options.type_expe:
    print 'erreur : type d\'experience non renseigne'
    enter_expe=raw_input("Nom du type d'experience que vous voulez renseigner :\n")
else:
    enter_expe=options.type_expe

experiment=Expe_type.objects.filter(name=enter_expe)
#message d'erreur si experiment vide


"""donnees d'experience biochimique
type d'experience en parametre (le type doit deja etre cree)
Demander d'entrer les variables dans le script comme pour les process : fonction define_variable
Demander dans le script si l'utilisateur veut creer des echantillons (si oui, les creer dans la base et les renseigner directement dans le fichier)
Demander dans les options : ingredient d'origine ? echantillon d'origine ?
Demander si le meme experimentateur a fait tous les echantillons

format :
nom de l'echantillon utilise (si plusieurs echantillons dans la meme expe, ils feront plusieurs lignes dans le fichier)
type d'experience
methode de mesure
description de la methode
projet de l'experience (encore non implemente dans la base
debut de l'expe
duree de l'expe
quantite d'enchantillon utilisee
(nom experimentateur
valeur de la variable
unite de la variable
methode associee a la donnee
date et heure de mesure) X nb_variable
"""











